import reducers from '../Reducers'
import { createStore, applyMiddleware } from 'redux'
import reduxThunk from 'redux-thunk'
import reduxLogger from 'redux-logger'
import { composeWithDevTools } from 'redux-devtools-extension'
import monitorReducersEnchancer from './enchancers'

const configureStore = (preloadStates) => {
    const middleWares = [reduxThunk, reduxLogger]
    const middleWareEnchancer = applyMiddleware(...middleWares)

    const enchancers = [middleWareEnchancer, monitorReducersEnchancer]
    const composedEnhancers = composeWithDevTools(...enchancers)

    const store = createStore(reducers, composedEnhancers)

    return store
}

export default configureStore