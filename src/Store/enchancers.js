
// const round = number => Math.round(number * 100) / 100
const reducerEnchancers = createStore => (
    reducer,
    initailStates,
    enchancer
) => {
    const monitoredReducers = (state, action) => {
        const newState = reducer(state, action)
        return newState
    }
    return createStore(monitoredReducers, initailStates, enchancer)
}

export default reducerEnchancers