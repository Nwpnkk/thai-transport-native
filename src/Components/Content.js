import React from 'react'
import { View } from 'react-native'
import { colors } from '../Utils'
import Styled from 'styled-components'


const Content = Styled(View)`
    flex: 1;
    width: 100%;
    paddingHorizontal: 16px;
    paddingVertical: ${props => props.vertical || 0}px;
    zIndex: -1;
    backgroundColor: ${props => props.backgroundColor || colors.backgroundPrimary};
`
const index = props => {
    return (
        <Content {...props} >
            {props.children}
        </Content>
    )
}

export default index