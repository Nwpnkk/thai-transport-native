import React from 'react'
import { View, TouchableOpacity, Text } from 'react-native'
import Styled from 'styled-components'
import { colors } from '../Utils'
import { widthPercentageToDP as vw } from 'react-native-responsive-screen'

const AppButton = Styled.TouchableOpacity`
    height: ${props => props.height || 52}px;
    margin: ${props => props.margin || 20}px;
    minWidth: ${props => props.minWidth || 150};
    backgroundColor: ${props => props.buttonColor || colors.primary};
    borderRadius: 30;
    display: flex;
    padding: 0px ${props => props.padding || 20}px;
    justifyContent: center;
    alignItems: center;
`

const Button = (props) => {
    return (
        <AppButton {...props}>
            <Text style={{ fontSize: props.fontSize || 20, fontWeight: "400", color: props.textColor || colors.white }}>{props.text}</Text>
        </AppButton>
    )
}

export default Button