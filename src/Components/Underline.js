import React from 'react'
import { View } from 'react-native'
import { colors } from '../Utils'
import Styled from 'styled-components'

const Underline = Styled.View`
    width: 100%;
    height: ${props => props.height || 0.5};
    backgroundColor: ${props => props.color || colors.lightGray};
    marginVertical: ${props => props.vertical || 2};
`

const index = ({ height, color, vertical = 0 }) => (
    <Underline height={height} color={color} vertical={vertical} />
)

export default index