import React from 'react'
import { TextInput, Platform } from 'react-native'
import { colors } from '../utils'
import Styled from 'styled-components'
import { heightPercentageToDP as vh, widthPercentageToDP as vw } from 'react-native-responsive-screen'

const InputField = Styled(TextInput)`
    fontWeight: ${Platform.OS === 'ios' ? 500 : 'normal'};
    width: ${props => props.width || vw(85)}
    fontSize: 16;
    paddingHorizontal: 14px;
    height: ${props => props.height || 45};
    marginVertical: 8px;
    borderRadius: 4;
    borderWidth: 1.4;
    borderColor: ${props => props.borderColor || colors.black};
    backgroundColor: ${props => props.backgroundColor || colors.white};
    textAlignVertical: top;
`

export default InputField
