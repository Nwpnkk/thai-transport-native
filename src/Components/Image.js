import React from 'react'
import styled from "styled-components/native"

const ImageComponent = styled.Image`
    width:${props => props.width ? props.width : 120};
    height:${props => props.height ? props.height : 120};
    resizeMode: ${props => props.resizeMode || 'contain'};
    ${props => props.alignItems ? `alignItems: ${props.alignItems}` : ''}
`

export default (props) =>
    <ImageComponent height={props.height} width={props.width} {...props} />
