import React from 'react'
import { View } from 'react-native'
import { colors } from '../Utils'
import styles from 'styled-components'


const Container = styles(View)`
    flex: 1;
    backgroundColor: ${props => props.backgroundColor || colors.backgroundPrimary};
`

export default Container