import React from 'react'
import { Text } from 'react-native'
import Styled from 'styled-components'
import PropTypes from 'prop-types'
import { colors } from '../utils'

const TextLabel = Styled(Text)`
    color: ${props => props.color || colors.textGray};
    fontSize: ${props => props.fontSize || 16};
    fontWeight: ${props => props.fontWeight || 500};
    marginVertical: 4;
    textAlign: ${props => props.textAlign || "left"};
`
export default TextLabel

TextLabel.propsTypes = {
    fontWeight: PropTypes.number,
    fontSize: PropTypes.number,
    color: PropTypes.string,
    textAlign: PropTypes.string
}