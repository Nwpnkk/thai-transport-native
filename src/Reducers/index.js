import { combineReducers } from 'redux'
import languageReducer from './lang'


const reducers = combineReducers({
    language: languageReducer
})

export default reducers