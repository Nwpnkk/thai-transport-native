
import { CHANGE_LANGUAGES } from '../Utils/constants'

export default (state = 'en', action) => {
    switch (action.type) {
        case CHANGE_LANGUAGES:
            return action.language
        default:
            return state

    }
}