import React from 'react'
import { View, Platform, StatusBar } from 'react-native'
import { colors } from './Utils'
import { Provider } from 'react-redux'
import configureStore from './Store/index'
import AsyncStorage from '@react-native-community/async-storage'

let lang, preloadState = {}

const response = AsyncStorage.getItem('lang')
response.then(result => { lang = result })
if (lang)
    preloadState.language = lang
const store = configureStore(preloadState)

export default class MainApp extends React.Component {
    constructor(props) {
        super(props)
    }

    render() {
        return (
            <Provider store={store}>
                <StatusBar barStyle="dark-content" backgroundColor={colors.white} />
                {/* <AppNavigator /> */}
                <View style={{ flex: 1, backgroundColor: colors.white }} />
            </Provider>

        )
    }
}
