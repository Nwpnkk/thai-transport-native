const colors = {
    primary: '#fe6512',
    secondary: '#0076c3',
    backgroundPrimary: '#ffffff',
    backgroundSecondary: '#ececec',
    white: '#ffffff',
    creamWhite: '#fff9ed',
    gray: '#dfdfdf',
    lightGray: '#d7d7d7',
    textGray: "#7a7a7a",
    darkGray: '#434343',
    lightBlack: '#252525',
    black: '#000000',
    green: '#42a444',
    darkGreen: '#10cd78',
    lightGreen: '#c9e394',
    red: '#ee3838',
    darkRed: '#f53031',
    lightRed: '#fde3e3',
    blue: '#1ca0e3',
    darkBlue : "#002046",
    blackTransparent : "rgba(0,0,0,0.79)"
}

export default colors