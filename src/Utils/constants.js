export const CHANGE_LANGUAGES = "CHANGE_LANGUAGES"

export const LOGIN_REQUEST = "LOGIN_REQUEST"
export const LOGIN_SUCCESS = "LOGIN_SUCCESS"
export const LOGIN_ERROR = "LOGIN_ERROR"

export const LOGOUT_SUCCESS = "LOGOUT_SUCCESS"
export const LOGOUT_REQUEST = "LOGOUT_REQUEST"
export const LOGOUT_ERROR = "LOGIN_ERROR"

export const FETCH_PROFILE = "FETCH_PROFILE"
export const FETCH_PROFILE_ERROR = "FETCH_PROFILE_ERROR"
export const FETCH_PROFILE_SUCCESS = "FETCH_PROFILE_SUCCESS"