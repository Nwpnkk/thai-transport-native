import thai from './th'
import english from './en'

const languages = {
    th: thai,
    en: english
}

export default languages